import axios, { AxiosInstance, AxiosResponse } from 'axios';
import { 
    CryptoData, 
    ConvertData, 
    Error404, 
    formatCurrency, 
    formatConvert 
} from '../types/currency';
import dotenv from 'dotenv';

dotenv.config();

const API_KEY: string = process.env.API_KEY ?? '';

export default class KenCrypto {
    baseURL: string = 'https://pro-api.coinmarketcap.com/';
    axiosInstance: AxiosInstance;
    
    headers: Readonly<Record<string, string | boolean>> = {
        Accept: "application/json",
        "X-CMC_PRO_API_KEY": API_KEY
    };

    constructor() {
        this.axiosInstance = axios.create({
            baseURL: this.baseURL,
            headers: this.headers
        });
    }

   async quotes(symbol: Array<string>) {
        const requestURL: string = `v1/cryptocurrency/quotes/latest?symbol=${symbol.toString()}`;

        try {
            const response: AxiosResponse<any, any> = await this.axiosInstance.get(requestURL);

            let data = await formatCurrency(response.data, symbol);

            return data as CryptoData;
        } catch (e) {
            if (axios.isAxiosError(e)) {
                return e.response?.data as Error404;
            }
            return undefined;
        }
        
    }

    async conversion(baseCurrency: string, amount: number, currencies: Array<string>) {
        const requestURL: string = `v1/tools/price-conversion?amount=${amount}&symbol=${baseCurrency}&convert=${currencies.toString()}`;

        try {
            const response: AxiosResponse<any, any> = await this.axiosInstance.get(requestURL);

            let data = await formatConvert(response.data);

            return data as ConvertData;
        } catch (e) {
            if (axios.isAxiosError(e)) {
                return e.response?.data as Error404;
            }
            return undefined;
        }
    }

}
