interface CDQInfo {
    price: number,
    last_updated: Date
}

interface CDQuote {
    [name: string]: CDQInfo
}

interface CDInfo {
    id: number,
    symbol: string,
    name: string,
    amount: number,
    last_updated: Date,
    quote: CDQuote
}

interface CDCurrency {
    [name: string]: CDInfo
}

export interface CryptoData {
    data: CDCurrency
}

interface CVQInfo {
    price: number,
    last_updated: Date
}

interface CVQuote {
    [name: string]: CVQInfo
}

interface CVInfo {
    id: number,
    symbol: string,
    name: string,
    amount: number,
    last_updated: Date,
    quote: CVQuote
}

export interface ConvertData {
    data: CVInfo
}

export interface Error404 {
    detail: string
}

export function isError404(error: any): error is Error404 {
    return (error as Error404).detail !== undefined;
}

export const formatCurrency = (data: any, symbols: Array<string>) => {
    const keys: Array<string> = ['num_market_pairs', 'tags', 'max_supply',
        'circulating_supply', 'total_supply', 'is_active', 'platform',
        'cmc_rank', 'is_fiat'];

    const qkeys: Array<string> = ['volume_24h', 'volume_change_24h',
        'percent_change_1h', 'percent_change_24h', 'percent_change_7d',
        'percent_change_30d', 'percent_change_60d', 'percent_change_90d', 
        'market_cap', 'market_cap_dominance', 'fully_diluted_market_cap'];

    delete data['status'];

    for (let i: number = 0; i < symbols.length; i++) {
        for (let j: number = 0; j < keys.length; j++) {
            delete data['data'][symbols[i]][keys[j]];
        } 
        for (let j: number = 0; j < qkeys.length; j++) {
            delete data['data'][symbols[i]]['quote']['USD'][qkeys[j]];
        }       
    }

    return data;
}

export const formatConvert = (data: any) => {
    delete data['status'];

    return data;
}
